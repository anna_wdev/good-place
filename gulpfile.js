var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var customizeBootstrap = require('gulp-customize-bootstrap');
var less = require('gulp-less');
var browserSync = require('browser-sync').create();

var cssSources = [
	'./dist/bootstrap.css',
	'./node_modules/select2/dist/css/select2.min.css',
	'./node_modules/bootstrap/dist/css/bootstrap-theme.css',
	'./node_modules/gridstack/dist/gridstack.css',
	'./node_modules/jssocials/dist/jssocials.css',
	'./node_modules/jssocials/dist/jssocials-theme-flat.css',
	'./node_modules/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
	'./node_modules/raty-js/lib/jquery.raty.css',
	'./node_modules/owl.carousel/dist/assets/owl.carousel.min.css',
	'./node_modules/owl.carousel/dist/assets/owl.theme.default.css',
	'./node_modules/fullcalendar/dist/fullcalendar.min.css',
	'./src/styles/plugins/jquery-ui.min.css',
	'./src/styles/plugins/roboto-fontface.css',
	'./src/styles/plugins/font-awesome.css',
	'./src/styles/plugins/fonts.css',
	'./src/styles/main.scss'
];

var jsSources = [
	'./node_modules/jquery/dist/jquery.js',
	'./node_modules/select2/dist/js/select2.full.js',
	'./node_modules/bootstrap/dist/js/bootstrap.js',
	'./node_modules/lodash/index.js',
	'./node_modules/gridstack/dist/gridstack.js',
	'./node_modules/jssocials/dist/jssocials.js',
	'./node_modules/moment/moment.js',
	'./node_modules/moment/locale/ru.js',
	'./node_modules/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
	'./node_modules/raty-js/lib/jquery.raty.js',
	'./node_modules/owl.carousel/dist/owl.carousel.min.js',
	'./node_modules/fullcalendar/dist/fullcalendar.min.js',
	'./node_modules/fullcalendar/dist/locale-all.js',
    './node_modules/masonry-layout/dist/masonry.pkgd.min.js',
	'./src/scripts/plugins/jquery-ui.min.js',
	'./src/scripts/plugins/jquery.simplePagination.js',
	'./src/scripts/search-component.js',
	'./src/scripts/good-place-app.js',
	'./src/scripts/main.js'
];

gulp.task('default', ['sass', 'js']);
gulp.task('production', ['sass:production', 'js:production']);

gulp.task('sass:production', ['compileBootstrap'], function() {
	return gulp.src(cssSources)
		.pipe(sass().on('error', sass.logError))
		.pipe(concat('main.css'))
		.pipe(gulp.dest('./dist'))
		.pipe(browserSync.stream());
});

gulp.task('sass', ['compileBootstrap'], function() {
	return gulp.src(cssSources)
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(concat('main.css'))
		.pipe(sourcemaps.write('./maps/css'))
		.pipe(gulp.dest('./dist'))
		.pipe(browserSync.stream());
});

gulp.task('compileBootstrap', function() {
	return gulp.src('./node_modules/bootstrap/less/bootstrap.less')
		.pipe(customizeBootstrap('./src/styles/less/*.less'))
		.pipe(less())
		.pipe(gulp.dest('./dist'));
});

gulp.task('js:production', function() {
	return gulp.src(jsSources)
		.pipe(concat('scripts.js'))
		.pipe(gulp.dest('./dist'))
		.pipe(browserSync.stream());
});

gulp.task('js', function() {
	return gulp.src(jsSources)
		.pipe(sourcemaps.init())
		.pipe(concat('scripts.js'))
		.pipe(sourcemaps.write('./maps/js'))
		.pipe(gulp.dest('./dist'))
		.pipe(browserSync.stream());
});

gulp.task('sass:watch', function() {
	gulp.watch('./src/styles/**/*.scss', ['sass']);
});

gulp.task('js:watch', function() {
	gulp.watch('./src/scripts/**/*.js', ['js']);
});

gulp.task('server', ['sass', 'js'], function() {
	// Serve files from the root of this project
	browserSync.init({
		server: {
			baseDir: './'
		}
	});

	gulp.watch('./src/styles/**/*.scss', ['sass']);
	gulp.watch('./src/scripts/**/*.js', ['js']);
	gulp.watch('./**/*.html').on('change', browserSync.reload);
});