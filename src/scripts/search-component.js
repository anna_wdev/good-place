var SearchComponent = (function() {
	var defaults = {
		selectors: {
			navSearch: '.js-nav-search',
			navIconSearch: '.js-nav-icon-search'
		},
		classNames: {
			expanded: 'expanded'
		}
	};
	var settings = {};

	/**
	 * Initialize
	 */
	function init() {
		$.extend(true, settings, defaults);

		attachEvents();
	}

	/**
	 * Attaches events
	 */
	function attachEvents() {
		$(settings.selectors.navIconSearch).click(function(event) {
			event.stopPropagation();

			$(settings.selectors.navSearch).toggleClass(settings.classNames.expanded);
		});

		$(settings.selectors.navSearch).on('click touchend', function(event) {
			event.stopPropagation();
		});

		$('body').on('click', function() {
			var $navSearch = $(settings.selectors.navSearch);
			var isExpanded = $navSearch.hasClass(settings.classNames.expanded);

			if (isExpanded) {
				$(settings.selectors.navSearch).toggleClass(settings.classNames.expanded, false);
			}
		});
	}

	/**
	 * @exports
	 */
	return {
		init: init
	}
})();