var GoodPlaceApp = (function() {
	var defaults = {
		selectors: {
			select2: '.js-select2',
			select2Multiple: '.js-select2-multiple',
			select2MultipleItem: '.filter-label',
			select2Sorting: '.js-select2-sorting',
			dateTimePicker: '#datetimepicker4',
			dateTimePickerModal: '#datetimepicker-modal',
			dateTimePicker5: '#datetimepicker5',
			dateTimePicker6: '#datetimepicker6',
			gridStack: '.js-grid-stack',
			gridStackItem: '.grid-stack-item',
			socialBtns: '.js-share-plugin',
			createAccountBtn: '.js-create-account-btn',
			loginBtn: '.js-login-btn',
			loginFormEntry: '.js-login-form-entry',
			registrationFormEntry: '.js-registration-form-entry',
			registrationSide: '.js-registration-side',
			loginSide: '.js-login-side',
			registrationFormBlock: '.js-registration-form-block',
			headerDatePickerBlock: '.js-header-date-time-picker',
			modalDatePickerBlock: '.js-modal-date-time-picker',
			settingsDatePickerTo: '.js-settings-date-time-picker-to',
			settingsDatePickerFrom: '.js-settings-date-time-picker-from',
			priceRange: '.js-price-range',
			averageAmountFrom: '#average-amount-from',
			averageAmountTo: '#average-amount-to',
			sliderLowerPrice: '.ui-slider-lower-price',
			sliderUpperPrice: '.ui-slider-upper-price',
			showExtendedFormButton: '.js-show-extended-form-btn',
			hideExtendedFormButton: '.js-hide-extended-form-btn',
			extendedForm: '.js-extended-form',
			starRating: '.js-star-rating',
			starRatingCurrentScore: '.js-star-rating-current-score',
			pagination: '.js-pagination',
			owlCarousel: '.js-owl-carousel',
			owlCarouselModal: '.js-owl-carousel-modal',
			carouselModal: '#carouselModal',
			changePasswordBtn: '.js-change-password-btn',
			changePasswordFields: '.js-change-password-fields',
			calendar: '#js-calendar'
		},
		classNames: {
			leftFormAnimation: 'left-form-animation',
			rightFormAnimation: 'right-form-animation',
			hidden: 'hidden',
			sliderLowerPrice: 'ui-slider-lower-price',
			sliderUpperPrice: 'ui-slider-upper-price'
		}
	};
	var settings = {};

	function init(options) {
		options = options || {};

		$.extend(true, settings, defaults, options);

		attachBootstrapNavHandler();
	}
    /**
     * Attaches select2 handler
     */
    function attachMasonry() {
        $('.grid').masonry({
            // options...
            itemSelector: '.poster-item',
            columnWidth: '.poster-item',
            gutter: 40
        });
    }
	/**
	 * Attaches select2 handler
	 */
	function attachSelect2Handler() {
		// Init select2 plugin with custom styling
		$(settings.selectors.select2).select2({
			minimumResultsForSearch: Infinity,
			theme: 'orange'
		});
	}

	/**
	 * Attaches select2 multiple handler
	 */
	function attachSelect2MultipleHandler() {
		var $select2Multiple = $(settings.selectors.select2Multiple)
			.select2({
				minimumResultsForSearch: Infinity,
				allowClear: true,
				theme: 'gray',
				width: '100%'
			})
			.on('select2:unselecting', function(e) {
				$(this).data('state', 'unselected');
			})
			.on('select2:open', function(e) {
				if ($(this).data('state') === 'unselected') {
					$(this).removeData('state');

					var self = $(this);
					setTimeout(function() {
						self.select2('close');
					}, 1);
				}
			});

		$(settings.selectors.select2MultipleItem).on('click', function(event) {
			var id = $(event.target).attr('for');

			$select2Multiple
				.filter('#' + id)
				.select2('open');
		});
	}

	/**
	 * Attaches select2 handler
	 */
	function attachSelect2SortingHandler() {
		$(settings.selectors.select2Sorting).select2({
			minimumResultsForSearch: Infinity,
			theme: 'sorting'
		});
	}

	/**
	 * Attaches datetimepicker
	 */
	function attachDateTimePicker() {
		// Bootstrap Datepicker
		$(settings.selectors.dateTimePicker).datetimepicker({
			format: 'DD/MM/YYYY',
			locale: 'ru',
			defaultDate: new Date()
			// debug:true
		});

		$(settings.selectors.headerDatePickerBlock).on('click', function() {
			$(settings.selectors.dateTimePicker).data('DateTimePicker').show();
		});

	}

	function attachDateTimePicker6() {
		// Bootstrap Datepicker
		$(settings.selectors.dateTimePicker6).datetimepicker({
			format: 'DD/MM/YYYY',
			locale: 'ru',
			defaultDate: new Date()
			// debug:true
		});


		$(settings.selectors.settingsDatePickerTo).on('click', function() {
			$(settings.selectors.dateTimePicker6).data('DateTimePicker').show();
		});

	}

	function attachDateTimePicker5() {
		// Bootstrap Datepicker
		$(settings.selectors.dateTimePicker5).datetimepicker({
			format: 'DD/MM/YYYY',
			locale: 'ru',
			defaultDate: new Date()
			// debug:true
		});

		$(settings.selectors.settingsDatePickerFrom).on('click', function() {
			$(settings.selectors.dateTimePicker5).data('DateTimePicker').show();
		});
	}

	/**
	 * Attaches datetimepicker in modal window
	 */
	function attachDateTimePickerInModal() {
		// Bootstrap Datepicker
		$(settings.selectors.dateTimePickerModal).datetimepicker({
			format: 'DD/MM/YYYY',
			locale: 'ru',
			defaultDate: new Date()
			// debug:true
		});

		$(settings.selectors.modalDatePickerBlock).on('click', function() {
			$(settings.selectors.dateTimePickerModal).data('DateTimePicker').show();
		});
	}

	/**
	 * Attaches gridstack handler
	 */
	function attachGridStackHandler() {
		// Gridstack plugin
		$(settings.selectors.gridStack).gridstack({
			cellHeight: 60,
			verticalMargin: 20,
			minWidth: 767
		});

		// Gridstack item link
		$(settings.selectors.gridStackItem).on('click', function() {
			var link = $(this).attr('data-href');
			window.location.href = link;
		});
	}

	/**
	 * Attaches social buttons handler
	 */
	function attachSocialButtonsHandler() {
		// jsSocials Plugin
		$(settings.selectors.socialBtns).jsSocials({
			showLabel: false,
			showCount: false,
			shares: ['facebook', 'pinterest', 'twitter']
		});
	}

	/**
	 * Attaches social buttons handler registration modal
	 */
	function attachSocialButtonsRegisterForm() {
		// jsSocials Plugin
		$(settings.selectors.socialBtns).jsSocials({
			showLabel: false,
			showCount: false,
			shares: ['pinterest', 'twitter', 'facebook']
		});
	}

	/**
	 * Attaches BS navbar handler
	 */
	function attachBootstrapNavHandler() {
		$('#bs-example-navbar-collapse-1')
			.on('shown.bs.collapse', function() {
				$('#navbar-hamburger').addClass(settings.classNames.hidden);
				$('#navbar-close').removeClass(settings.classNames.hidden);
				$('.navbar-menu-customized').addClass('open');
			})
			.on('hidden.bs.collapse', function() {
				$('#navbar-hamburger').removeClass(settings.classNames.hidden);
				$('#navbar-close').addClass(settings.classNames.hidden);
				$('.navbar-menu-customized').removeClass('open');
			});
	}

	/**
	 * Attaches registration form events
	 */
	function attachRegistrationFormEvents() {
		$(settings.selectors.createAccountBtn).on('click', function() {
			$(settings.selectors.registrationSide + ',' + settings.selectors.loginFormEntry)
				.addClass(settings.classNames.hidden);

			$(settings.selectors.loginSide + ',' + settings.selectors.registrationFormEntry)
				.removeClass(settings.classNames.hidden);

			$(settings.selectors.registrationFormBlock)
				.addClass(settings.classNames.leftFormAnimation)
				.removeClass(settings.classNames.rightFormAnimation);
		});

		$(settings.selectors.loginBtn).on('click', function() {
			$(settings.selectors.loginSide + ',' + settings.selectors.registrationFormEntry)
				.addClass(settings.classNames.hidden);

			$(settings.selectors.registrationSide + ',' + settings.selectors.loginFormEntry)
				.removeClass(settings.classNames.hidden);

			$(settings.selectors.registrationFormBlock)
				.addClass(settings.classNames.rightFormAnimation)
				.removeClass(settings.classNames.leftFormAnimation);
		});
	}

	/**
	 * Show/hide extended form
	 */
	function toggleExtendedForm() {
		$(settings.selectors.showExtendedFormButton).on('click', function() {
			$(this).addClass(settings.classNames.hidden);
			$(settings.selectors.extendedForm)
				.removeClass(settings.classNames.hidden)
				.slideDown(1000);
		});

		$(settings.selectors.hideExtendedFormButton).on('click', function() {
			$(settings.selectors.showExtendedFormButton).removeClass(settings.classNames.hidden);
			$(settings.selectors.extendedForm)
				.addClass(settings.classNames.hidden)
				.slideUp(1000);
		});
	}

	/**
	 * Attaches filter price range
	 */
	function attachPriceRangeHandler() {
		var lower = 150;
		var upper = 5000;

		$(settings.selectors.priceRange).slider({
			range: true,
			min: 0,
			max: 7000,
			step: 50,
			values: [lower, upper],
			slide: function(event, ui) {
				$(settings.selectors.averageAmountFrom).val(ui.values[0]);
				$(settings.selectors.averageAmountTo).val(ui.values[1]);
				$(settings.selectors.sliderLowerPrice).text(ui.values[0]);
				$(settings.selectors.sliderUpperPrice).text(ui.values[1]);
			},
			create: function(event, ui) {
				$(this)
					.find('.ui-slider-handle:first')
					.append('<span class="' + settings.classNames.sliderLowerPrice + '">' + lower + '</span>')
					.end()
					.find('.ui-slider-handle:last')
					.append('<span class="' + settings.classNames.sliderUpperPrice + '">' + upper + '</span>');

				$(settings.selectors.averageAmountFrom)
					.val(lower);

				$(settings.selectors.averageAmountTo)
					.val(upper);

				$(settings.selectors.averageAmountFrom).change(function() {
					$(settings.selectors.priceRange).slider('values', 0, $(this).val());
				});

				$(settings.selectors.averageAmountTo).change(function() {
					$(settings.selectors.priceRange).slider('values', 1, $(this).val());
				});
			},
			change: function(event, ui) {
				$(this)
					.find(settings.selectors.sliderLowerPrice)
					.text(ui.values[0])
					.end()
					.find(settings.selectors.sliderUpperPrice)
					.text(ui.values[1]);
			}
		});
	}

	/**
	 * Attaches Raty JS star rating
	 */
	function attachStarRatingHandler() {
		$(settings.selectors.starRating).raty({
			score: 3.5,
			number: 5,
			starHalf: 'images/star-half.png',
			starOff: 'images/star-off.png',
			starOn: 'images/star-on.png'
		});
	}

	/**
	 * Attaches Simple Pagination Plugin
	 */
	function attachPaginationHandler() {
		$(settings.selectors.pagination).pagination({
			items: 25,
			displayedPages: 8,
			edges: 1,
			currentPage: 1,
			cssStyle: '',
			prevText: '<span><i class="fa fa-angle-left" aria-hidden="true"></i></span>',
			nextText: '<span><i class="fa fa-angle-right" aria-hidden="true"></i></span>'
		});
	}

	/**
	 * Attaches jQuery LightSlider
	 */
	function attachGallerySliderHandler() {
		$(settings.selectors.owlCarousel).owlCarousel({
			loop: true,
			margin: 10,
			dots: false,
			autoHeight: true,
			nav: true,
			navText: ['<span><i class="fa fa-angle-left" aria-hidden="true"></i></span>','<span><i class="fa fa-angle-right" aria-hidden="true"></i></span>'],
			responsive: {
				0: {
					items:1
				},
				768: {
					items:3
				}
			},
			onRefresh: function () {
				$(settings.selectors.owlCarousel)
					.find('div.owl-item img').height('');
			},
			onRefreshed: function () {
				$(settings.selectors.owlCarousel)
					.find('div.owl-item img')
					.height($(settings.selectors.owlCarousel).height());
			}
		});

		$(settings.selectors.carouselModal).on('show.bs.modal', function() {
			$(settings.selectors.owlCarouselModal).owlCarousel({
				items: 1,
				loop: true,
				margin: 10,
				dots: false,
				autoHeight: true,
				nav: true,
				navText: ['<span><i class="fa fa-angle-left" aria-hidden="true"></i></span>','<span><i class="fa fa-angle-right" aria-hidden="true"></i></span>']
			});
		});
	}

	/**
	 * Initializing Bootstrap Tooltip
	 */
	function initialBsTooltip() {
		$('[data-toggle="tooltip"]').tooltip();
	}

	/**
	 * Change password function
	 */
	function changePassword() {
		$(settings.selectors.changePasswordBtn).on('click', function(e) {
			e.preventDefault();
			$(settings.selectors.changePasswordFields).removeClass(settings.classNames.hidden);
		})
	}

	/**
	 * Attaches fullcalendar plugin
	 */
	function attachFullCalendar() {
		$(settings.selectors.calendar).fullCalendar({
			defaultView: 'agendaWeek',
			header: {
				left:   'agendaWeek,agendaDay',
				center: '',
				right:  'prev, title, next'
			},
			navLinks: true,
			editable: true,
			locale: 'uk',
			titleFormat: 'D MMMM YYYY',
			selectable: true,
			eventLimit: true,
			events: [
				{
					title  : 'event1',
					start  : '2017-10-10'
				},
				{
					title  : 'event2',
					start  : '2017-10-11',
					end    : '2017-10-12'
				},
				{
					title  : 'event3',
					start  : '2017-10-13T12:30:00',
					allDay : false
				}
			]
		});
	}

	/**
	 * Attaches home page events handlers
	 */
	function attachHomePageEvents() {
		SearchComponent.init();
		attachSelect2Handler();
		attachDateTimePicker();
		attachGridStackHandler();
		attachSocialButtonsHandler();
	}

	/**
	 * Attaches registration page events handlers
	 */
	function attachRegistrationModalEvents() {
		attachSocialButtonsRegisterForm();
		attachRegistrationFormEvents();
	}

	/**
	 * Attaches filter page events handlers
	 */
	function attachFilterPageEvents() {
		SearchComponent.init();
		attachSelect2Handler();
		attachSelect2MultipleHandler();
		attachSelect2SortingHandler();
		attachDateTimePicker();
		attachDateTimePickerInModal();
		attachPriceRangeHandler();
		toggleExtendedForm();
		attachStarRatingHandler();
		attachPaginationHandler();
		attachSocialButtonsHandler();
		initialBsTooltip();
	}

	/**
	 * Attaches restaurant page events handlers
	 */
	function attachRestaurantPageEvents() {
		SearchComponent.init();
		attachDateTimePicker();
		attachSelect2Handler();
		attachGridStackHandler();
		attachSocialButtonsHandler();
		attachGallerySliderHandler();
		changePassword();
	}
	/**
	 * Attaches restaurant settings page events handlers
	 */
	function attachRestaurantSettingsPageEvents() {
		SearchComponent.init();
		attachDateTimePicker5();
		attachDateTimePicker6();
		attachSelect2Handler();
		attachSelect2MultipleHandler();
		attachSelect2SortingHandler();
		attachSocialButtonsHandler();
	}

	function attachRestManagerAllReservePageEvents() {
		SearchComponent.init();
		attachSelect2SortingHandler();
		attachDateTimePicker();
		toggleExtendedForm();
		attachPaginationHandler();
		attachSocialButtonsHandler();
		attachFullCalendar();
	}

	/**
	 * Attaches 404 page events handlers
	 */
	function attach404PageEvents() {
		SearchComponent.init();
		attachSelect2Handler();
	}
	/**
	 * Attaches Poster page events handlers
	 */
	function attachPosterPageEvents() {
        attachMasonry();
	}

	/**
	 * @exports
	 */
	return {
		init: init,
		attachHomePageEvents: attachHomePageEvents,
		attachRegistrationModalEvents: attachRegistrationModalEvents,
		attachFilterPageEvents: attachFilterPageEvents,
		attachRestaurantPageEvents: attachRestaurantPageEvents,
		attach404PageEvents: attach404PageEvents,
		attachRestaurantSettingsPageEvents: attachRestaurantSettingsPageEvents,
		attachRestManagerAllReservePageEvents: attachRestManagerAllReservePageEvents,
        attachPosterPageEvents: attachPosterPageEvents
	}
})();